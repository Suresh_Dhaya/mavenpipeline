<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Policy Details</title>
</head>
<body>

<h2>Employee Details - Search Results</h2>
	<table>
	<tr><td>Employee Id</td> <td> ${employee.empId}</td></tr>
	<tr><td>Employee Name</td> <td>${employee.empName }</td></tr>
	<tr><td>Salary</td> <td> ${employee.salary }</td></tr>
	<tr><td>Location</td> <td>${employee.location }</td></tr>
	<tr><td>Department Name</td> <td>${employee.deptName }</td></tr>
	<tr><td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
</body>
</html>