<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	
	<div align="right"><a href="/springbootdemo/logout">Logout</a></div>
	<h2>Spring Boot Customer Insurance Application - Spring JPA</h2>
	<a href="customerForm" >Customer Form</a><br/><br/>
	<a href="issuePolicyForm" >Issue Policy Form </a><br/><br/>
	<a href="searchPolicyForm" >Search Policy Form </a> <br/><br/>
	<a href="searchCustomerForm">Take new policy</a>  <br/><br/>
	<a href="searchCustomerPolicy" >Search Customer Policies </a>  <br/><br/>
	
	<h2>Spring Boot Customer Insurance Application - Spring JDBC</h2>
	
	<a href="customerFormDAO" >Customer Form</a><br/><br/>
	<a href="issuePolicyFormDAO" >Issue Policy Form </a><br/><br/>
	<a href="searchPolicyFormDAO" >Search Policy Form </a> <br/><br/>
	<a href="searchCustomerFormDAO">Take new policy</a>  <br/><br/>
	<a href="searchCustomerPolicyDAO" >Search Customer Policies </a>  <br/><br/>
	
	<h3>Spring boot Employee Department Application - REST Service</h3>
	
	<a href="addEmployeeForm">Add Employee</a><br/><br/>
	<a href="searchEmployeeForm">Search Employee</a><br/><br/>
	<a href="getAllEmployees">List All Employees</a><br/><br/>
	<a href="searchDepartment">List All Employees in a Department</a><br/><br/>	
	<a href="removeEmployeeForm">Remove Employee</a><br/><br/>
	
</body>
</html>