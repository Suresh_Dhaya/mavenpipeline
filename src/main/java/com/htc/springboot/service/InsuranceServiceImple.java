package com.htc.springboot.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.springboot.model.Customer;
import com.htc.springboot.model.Policy;
import com.htc.springboot.repository.CustomerRepository;
import com.htc.springboot.repository.PolicyRepository;
import com.htc.springboot.to.CustomerTO;
import com.htc.springboot.to.PolicyTO;


@Service
public class InsuranceServiceImple implements InsuranceService {

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	PolicyRepository policyRepository;
	
	private CustomerTO getCustomerTO(Customer customer) {
		CustomerTO customerTO = new CustomerTO();
		customerTO.setCustomerCode(customer.getCustomerCode());
		customerTO.setCustomerName(customer.getCustomerName());
		customerTO.setAddress(customer.getAddress());
		customerTO.setContactNo(customer.getContactNo());
		return customerTO;
	}
	private Customer getCustomer(CustomerTO customerTO) {
		Customer customer= new Customer();
		customer.setCustomerCode(customerTO.getCustomerCode());
		customer.setCustomerName(customerTO.getCustomerName());
		customer.setAddress(customerTO.getAddress());
		customer.setContactNo(customerTO.getContactNo());
		return customer;
	}
	
	private Policy getPolicy(PolicyTO policyTO) {
		Policy policy = new Policy();
		policy.setPolicyNo(policyTO.getPolicyNo());
		policy.setPremium(policyTO.getPremium());
		policy.setPolicyMode(policyTO.getPolicyMode());
		policy.setStartDate(policyTO.getStartDate());
		return policy;
	}
	
	private PolicyTO getPolicyTO(Policy policy) {
		PolicyTO policyTO =new PolicyTO();
		policyTO.setPolicyNo(policy.getPolicyNo());
		policyTO.setPremium(policy.getPremium());
		policyTO.setPolicyMode(policy.getPolicyMode());
		policyTO.setStartDate(policy.getStartDate());
		return policyTO;
	}
	@Override
	public boolean addCustomer(CustomerTO customerTO) {
		if(customerRepository.save(getCustomer(customerTO))!=null)
			return true;
		else 
			return false;
	}

	@Override
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		
		System.out.println(customer);
		System.out.println(policy);
		
		Set<Policy> policies= customer.getPolicies();
		if(policies ==null) {
			policies= new HashSet<Policy>();
		}
		policies.add(policy);
		customer.setPolicies(policies);
		policy.setCustomer(customer);
		
		customerRepository.save(customer);
		return true;
	}

	@Override
	public CustomerTO getCustomerDetails(String customerCode) {
		Optional<Customer> optCustomer = customerRepository.findById(customerCode);
		if(optCustomer.isPresent()) {
			return getCustomerTO(optCustomer.get());
		}
		return null;
	}
	@Override
	public PolicyTO getInsurancePolicy(long policyNo) {
		Optional<Policy> optPolicy = policyRepository.findById(policyNo);
		if(optPolicy.isPresent()) {
			return getPolicyTO(optPolicy.get());
		}
		return null;
	}
	@Override
	public boolean takeNewPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		
		System.out.println(customer);
		System.out.println(policy);
		policy.setCustomer(customer);
		policyRepository.save(policy);
		return true;
	}
	@Override
	public Set<Policy> getInsurancePolicies(String customerCode) {
		/*
		 * List<PolicyTO> policies=policyRepository.findByCustomer(customerCode);
		 * if(policies.isEmpty()) { return null; }
		 * System.out.println("in get policies"+policies); return policies;
		 */
		Optional<Customer> optCustomer = customerRepository.findById(customerCode);
		if(optCustomer.isPresent()) {
			Customer cust = optCustomer.get();
			return cust.getPolicies();
		}
		return null;
	}
}
