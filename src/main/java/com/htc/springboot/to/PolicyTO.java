package com.htc.springboot.to;

import java.util.Date;

public class PolicyTO {
	private long policyNo;
	private double premium;
	private String policyMode;
	private Date startDate;

	public PolicyTO() {
	}

	public PolicyTO(long policyNo, double premium, String policyMode, Date startDate) {
		super();
		this.policyNo = policyNo;
		this.premium = premium;
		this.policyMode = policyMode;
		this.startDate = startDate;
	}

	public long getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(long policyNo) {
		this.policyNo = policyNo;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public String getPolicyMode() {
		return policyMode;
	}

	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "PolicyTO [policyNo=" + policyNo + ", premium=" + premium + ", policyMode=" + policyMode + ", startDate="
				+ startDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((policyMode == null) ? 0 : policyMode.hashCode());
		result = prime * result + (int) (policyNo ^ (policyNo >>> 32));
		long temp;
		temp = Double.doubleToLongBits(premium);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PolicyTO other = (PolicyTO) obj;
		if (policyMode == null) {
			if (other.policyMode != null)
				return false;
		} else if (!policyMode.equals(other.policyMode))
			return false;
		if (policyNo != other.policyNo)
			return false;
		if (Double.doubleToLongBits(premium) != Double.doubleToLongBits(other.premium))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
}
